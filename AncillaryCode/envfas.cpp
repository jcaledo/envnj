#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <vector>
#include <list>
#include <algorithm>
using namespace std;



string concSeqs(string fastaFile, int r);
string envExtract(string seq, int c, int r); // Prescindible.
void posAa(vector<size_t> &vec, string protein, char A);
void aroundAa(int v_Aa[], string Protein, char A, int aaOrder, int DIAMETER);


int main(int argc, char* argv[]){

	// usage: ./envfas Ascoidea_asiatica.fasta 10 ./speciesVectors/v_Ascoidea_asiatica.txt

	// ------------------ Parameters ------------------ //
	string path = argv[1]; // path to fasta file
	string r = argv[2]; // radius
	string outfile = argv[3]; // path to output file
	// ------------------------------------------------ //
	int radius;
	stringstream ss;
	ss << r;
	ss >> radius;

	

	int dim = 20 * 2*radius * 20;
	#define DIM dim
	const int DIAMETER = 2*radius;
 
	// Path to the fasta file containing the species proteome
	string Protein = concSeqs(path, radius);
	// cout << Protein << endl;

	int v_Aa[DIM]; // environment vector for amino acids (20 x 4 x 20, for r = 2).

	for (int i = 0; i < DIM; i++){ // initialization of vector v_Aa.
		 v_Aa[i] = 0;
	}


	
	string aminoacidos = "ARNDCQEGHILKMFPSTWYV";
	for (int i = 0; i < 20; i++){ // For each amino acid.
		aroundAa(v_Aa, Protein, aminoacidos[i], i, DIAMETER);
	}

	// ---- Saving the species vector in a file.
	fstream tobesaved;
	tobesaved.open(outfile, ios::out);
	if (tobesaved.is_open()){
		for (int i = 0; i < DIM; i++){
			tobesaved << v_Aa[i] << "\n";
		}
		tobesaved.close();
	} else {
		cout << "Sorry the file " << outfile << " couldn't be opened!" << endl;
	}



/*
	Test: 12 environment sequences of length 4: 12 * 4 = 48
	int suma = 0;
	for (int i = 0; i < DIM; i++){
		// cout << i << " : " << v_Aa[i] << endl;
		suma += v_Aa[i];
	}
	cout << suma << endl; 
*/

	return 0;
}



// -------------------------------------------------------//
// ----------------  ANCILLARY FUNCTION ------------------//
// -------------------------------------------------------//


/* --------- concSeqs -------------------------------------

Reads a fasta file and concatenate all the individual 
sequences into a single sequence.

usage: concSeqs(fastaFile, r)

fastaFile: path to the fasta file to be concatenated.
r: # times 'X' is interleaved between different proteins.

It returns a string with the concatenated sequences.

------------------------------------------------------------ */
string concSeqs(string fastaFile, int r){

	ifstream archivo(fastaFile);

	if (!archivo.good()) {
        std::cerr << "Error opening: " << fastaFile << "  failed." << std::endl;
    }

    string line, Prot_sequence;
    while (std::getline(archivo, line)) {

        // line may be empty so you *must* ignore blank lines
        // or you have a crash waiting to happen with line[0]
        if(line.empty())
            continue;

        if (line[0] == '>')
            Prot_sequence += string(r, 'X');
        else
        	Prot_sequence += line;
    }

    Prot_sequence += string(r, 'X');

    return Prot_sequence;
}


/* --------- envExtract -------------------------------------

Extracts the sequence environment around a given position.

usage: env.extract(seq, c, r)

seq: a string protein sequence.
c: center of the environment.
r radius of the environment.

It returns a  a strings with the extracted environment sequence. 

--------------------------------------------------------------*/
string envExtract(string seq, int c, int r){

	seq = string(r, 'X') + seq + string(r, 'X'); // avoid out-of-range errors
	string environment;
	environment = seq.substr(c - 1, 2*r + 1);
	return environment; 

}


/* --------- posAa -------------------------------------

Get the postions where the amino acid 'A' is found in the
sequence of the 'Protein'.

usage: posAa(&vec, protein, A)

&vec: address of the vector (from main) that will contain the pos. 
Protein: string of the protein sequence.
A: amino acid to be analyzed.

It returns (to main) an array with the positions from protein 
where the amino acid A is present. 

--------------------------------------------------------------*/
void posAa(vector<size_t> &vec, string Protein, char A){

	size_t pos = Protein.find(A); // Get the first occurrence.	
	while (pos != string::npos){ // Repeat till end is reached.
		vec.push_back(pos); // Add position to the vector.
		pos = Protein.find(A, pos + 1); // Get the next occurrence from the current position.
	}
}


/* --------- aroundAa -------------------------------------

Computes the vector (20 x 2r) of frequencies around a given
amino acid 'A'.

usage: posAa(v_Aa[], Protein, A)

v_Aa[]: address of the vector (from main) containing the freq. 
Protein: string of the protein sequence.
A: amino acid to be analyzed.

It returns (to main) an array with the frequencies. 

--------------------------------------------------------------*/
void aroundAa(int v_Aa[], string Protein, char A, int aaOrder, int DIAMETER){

	int r = DIAMETER/2;
	// --------------- Define the absolute frequencies matrix M  -----------------
	//               environment of radius = 2 (r is hard-coded)  
	int M[20][DIAMETER];
		
	for (int i = 0; i <20; i++){
		for (int j = 0; j < DIAMETER; j++){
			M[i][j] = 0;
		}
	} // -------------------------------------------------------------------------

	vector<size_t> vec; // Position vector for the current amino acid A -----
	posAa(vec, Protein, A);


	if (vec.size() != 0){ // Only if the amino acid A is present in the sequence.
		string aminoacidos = "ARNDCQEGHILKMFPSTWYV";

		for (int i = 0; i < vec.size(); i++){ // position i for tha amino acid A.
			string seqEnv = Protein.substr(vec[i] - r, r) + Protein.substr(vec[i] + 1, r); // i-th env sequence (corresponding to the i-th pos).
			if (seqEnv.find('X') == string::npos){ // When the env seq doesn't contain X

				// cout << "Env Sequence Around " << A << " :  " << seqEnv << endl;

				for (int j = 0; j < DIAMETER; j++){ // iterate through the env seq positions
					M[ aminoacidos.find(seqEnv[j]) ] [j] += 1;	
				}		
			}
    	}
    	int k = 20 * DIAMETER * aaOrder;
    	for (int j = 0; j < DIAMETER; j++){
    		for (int i = 0; i < 20; i++){
    			v_Aa[k] = M[i][j];
    			k++;
    		}
    	}

	} else { // If the amino acid A is not present in the sequence.
		int k = 20 * DIAMETER * aaOrder;
    	for (int j = 0; j < DIAMETER; j++){
    		for (int i = 0; i < 20; i++){
    			v_Aa[k] = 0;
    			k++;
    		}
    	}
	}
   	
}