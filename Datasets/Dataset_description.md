# OmaSet.Rda
Dataframe (df) of 907 rows (one per orthologous protein sequence) and 5 columns (one per species + one additional column)
The entries of this df provide the protein entry instance. The corresponding sequence can be obtained in the dataframe named
oseq.Rda. To see how this dataset has been generated consult the R script Oma_PlantAnimal.R

# oseq.Rda
Protein sequences of 907 orthologous proteins found in the species: human, chimp, gorilla and arabidopsis (thaliana and lyrata). To see how this dataset has been generated consult the R script Oma_PlantAnimal.R

# bovids.rda
Dataframe containing the protein sequences of the 13 mtDNA-encoded mitochondrial proteins in 11 species of bovids.

# reyes.rda
Dataframe containing the protein sequences of the 13 mtDNA-encoded mitochondrial proteins in 34 mammalian species.

# PlantAnimals_fasta
Contains sequences from a random partition of non-homologous protein and DNA sequences in fasta format.